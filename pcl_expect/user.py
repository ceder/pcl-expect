# pcl-expect: expect for Python.  User interface.
# Copyright (C) 2003  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import atexit
import sys
import tty
import termios

import pcl_expect

__all__ = [
    "User",
    "RAW",
    "CBREAK",
    ]

RAW = 0
CBREAK = 1

_saved_mode = None

def _save():
    global _saved_mode

    if _saved_mode is None:
        _saved_mode = termios.tcgetattr(sys.stdin.fileno())

    atexit.register(_restore)

def _restore():
    global _saved_mode

    if _saved_mode is not None:
        termios.tcsetattr(sys.stdin.fileno(), termios.TCSAFLUSH, _saved_mode)
        _saved_mode = None

        

class User(pcl_expect.Expectable):
    def __init__(self, mode = None):

        fd = sys.stdin.fileno()

        if mode == CBREAK:
            _save()
            tty.setcbreak(fd)
        elif mode == RAW:
            _save()
            tty.setraw(fd)
        elif mode != None:
            raise pcl_expect.BadArgs()

        pcl_expect.Expectable.__init__(self, fd)
        
    def send(self, s):
        sys.stdout.write(s)
        sys.stdout.flush()

    def close(self):
        pcl_expect.Expectable.close(self)
        _restore()
