# pcl-expect: expect for Python.  TCP interface.
# Copyright (C) 2003  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""Connect to a TCP socket.

   This module provides the TcpClient class.  It inherits Expectable
   and can be used to communicate with a TCP/IP server.
"""

import socket

import pcl_expect

__all__ = [
    "TcpBase",
    "TcpClient",
    ]

class TcpBase(pcl_expect.Expectable):

    """Communicate with a remote TCP port."""

    def __init__(self, sock):
        """Communicate with sock, which should be a connected socket object.
        """

        self.sock = sock
        pcl_expect.Expectable.__init__(self, self.sock.fileno())

    def send(self, s):
        """Send a string to the remote TCP port."""
        self.sock.send(s)

    def close(self):
        """Close the session."""
        pcl_expect.Expectable.close(self)
        self.sock.close()

class TcpClient(TcpBase):

    """Connect to a remote TCP port."""

    def __init__(self, sockaddr):
        """Connect a TCP socket to the address given by sockaddr.

           sockaddr is passed to the connect() method of a socket
           object.  It should normally be a pair of a host name and
           port number.

        """

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.connect(sockaddr)
        TcpBase.__init__(self, sock)

# FIXME (bug 1180): It would be nice to provide a TcpServer class as
# well, but it wouldn't fit in the current Expectable framework.
