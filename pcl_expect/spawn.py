# pcl-expect: expect for Python.  Pty interface.
# Copyright (C) 2003  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""Spawn subprocesses via a pty.

   The normal way to use this is by creating an instance of the Spawn
   class.  This will connect the stdin, stdout and stderr of the new
   process to the pty.  You can then use the send() method to send
   data to the procces, and wait for input using the normal Expectable
   API.

   The module uses the pty module.  Not all systems support it.

   stdout vs stderr
   ================

   If you need to differentiate between stdout and stderr of the
   proces, you can use the spawn2() function.  This will create a
   pipe and a pty.  The pty will be connected to stdin and stdout of
   the process, while the pipe will be connected to stderr.  spawn2()
   returns two expectable objects, representing the pty and the pipe.

   The method of connecting stderr to a pipe is not perfect.  Some
   programs assumes that stderr is connected to the tty, and will not
   work properly if it is connected to a pipe.  For instance, if you
   start csh on Solaris 9 this way, it will not have job control
   features.  And if you start bash, and try to run stty from bash, it
   will fail (tested with bash-2.05 under Solaris 9 and Linux 2.4.22).
   As long as the program you start isn't a shell or a program that
   calls isatty(2) you should be fine, though.

   inherit_stty
   ============

   By default, the new pty will inherit the stty settings of stdin, if
   stdin is a tty.  You can disable this by setting the global module
   variable inherit_stty to False.  The setting of inherit_stty can
   also be overriden by the optional argument inherit_stty to the
   Spawn constructor or spawn2() function.

   stty_init
   =========

   After possibly inheriting the stty settings (see inherit_stty
   above) stty will be run on the new pty with the stty_init string as
   an argument.  stty_init is default False, which means that this
   step is skipped.  stty_init is also a global variable that can be
   overridden by optional arguments to the Spawn constructor or
   spawn2() function.

"""

import os
import sys
import pty
import errno
import fcntl

import pcl_expect
import pcl_expect.remote_exception

__all__ = [
    "inherit_stty",
    "stty_init",
    "Spawn",
    "spawn2",
    ]

inherit_stty = True
stty_init = False

def set_cloexec_flag(fd):
    """Set the FD_CLOEXEC flag on fd."""
    old = fcntl.fcntl(fd, fcntl.F_GETFD)
    fcntl.fcntl(fd, fcntl.F_SETFD, old | fcntl.FD_CLOEXEC)

def _use(a, b):
    """Return b if a is None, otherwise return a."""
    if a is None:
        return b
    else:
        return a

def _inherit_stty(override):
    """Return the inherit_stty setting to use."""
    _use(override, inherit_stty)

def _stty_init(override):
    """Return the stty_init setting to use."""
    _use(override, stty_init)


def _spawn(cmd, inherit_stty, stty_init, use_stderr_pipe):
    """Spawn a process running cmd.

       cmd -- A list of strings to be used as the argv vector of
              os.execvp.  cmd[0] will also be used as the program to
              run.  This can also be a string that will be split on
              space to produce this list.  There is no way to quote
              a space, so unless the string is a constant you should
              normally not use that form.

       inherit_stty -- True if the stty settings should be inherited
              if stdin is a tty.

       stty_init -- String to pass to stty to initialize the pty.

       use_stderr_pipe -- True if stderr of the new process should be
              connected to a separate pipe instead of to the pty.

    """

    inherit = inherit_stty and os.isatty(sys.stdin.fileno())
    if inherit:
        f = os.popen("stty -g")
        settings = f.read()
        f.close()

    if isinstance(cmd, str):
        cmd = cmd.split(" ")

    # Create a synchronization pipe, so that the parent doesn't
    # continue execution until the pty has been initialized and the
    # new process execed.  If all goes well, the parent will receive
    # an end-of-file on the pipe when the exec closes the pipe.  If
    # exec fails, the errno code will be sent via this pipe.
    (r, w) = os.pipe()
    set_cloexec_flag(w)

    if use_stderr_pipe:
        (stderr_r, stderr_w) = os.pipe()
    else:
        stderr_r = None

    pid, fd = pty.fork()
    if pid == 0:
        try:
            try:
                os.close(r)
                if inherit:
                    os.system("stty %s" % settings)
                if stty_init is not None:
                    os.system("stty %s" % stty_init)
                if use_stderr_pipe:
                    os.close(stderr_r)
                    os.dup2(stderr_w, 2)
                    os.close(stderr_w)

                os.execvp(cmd[0], cmd)
            except:
                os.write(w, remote_exception.serialize())
        finally:
            os._exit(1)

    # In parent.
    os.close(w)
    if use_stderr_pipe:
        os.close(stderr_w)
    res = os.fdopen(r).read()
    if res != "":
        os.close(fd)
        os.waitpid(pid, 0)
        remote_exception.re_raise(res)
    return pid, fd, stderr_r

class Spawn(pcl_expect.Expectable):
    """Talk to a subprocess via a pty.
    """

    def __init__(self, cmd, inherit_stty = None, stty_init = None,
                 use_stderr_pipe = False):
        """Spawn a process via a pty.

           Arguments: 

            - cmd -- A list of strings to be used as the argv vector
                  of os.execvp.  cmd[0] will also be used as the
                  program to run.  This can also be a string that will
                  be split on space to produce this list.  There is no
                  way to quote a space, so unless the string is a
                  constant you should normally not use that form.

           There are two documented optional arguments:

            - inherit_stty -- True if the stty settings should be
                  inherited if stdin is a tty.  This overrides the
                  global setting.

            - stty_init -- String to pass to stty to initialize the
                  pty.  This overrides the global setting.

           If os.execvp fails (or if anything else fails in the child
           process), a pcl_expect.remote_exception.Remote exception
           will be raised.

        """

        self.__child_pid, fd, stderr = _spawn(
            cmd,
            _inherit_stty(inherit_stty),
            _stty_init(stty_init),
            use_stderr_pipe)

        pcl_expect.Expectable.__init__(self, fd)
        if use_stderr_pipe:
            # The stderr attribute is explicitly not documented.
            # The only published API to get this information is via
            # the spawn2() function.
            self.stderr = pcl_expect.Expectable(stderr)

    def send(self, s):
        """Send a string to the process."""
        pcl_expect.debug("sending \"%s\" to fd %d" % (s, self.fileno()))
        os.write(self.fileno(), s)

    def _read(self):
        # When reading from a pty, at least under Linux, you get EIO
        # when the process dies.  Treat this as a normal end-of-file.
        try:
            return pcl_expect.Expectable._read(self)
        except OSError as err:
            if err.errno == errno.EIO:
                return "", True
            raise

    def kill(self, sig):
        """Send signal SIG to the child process."""
        os.kill(self.__child_pid, sig)

    def close(self):
        """Close the pty and wait for the child to die.

           Return the exit status, as reported by os.waitpid().

        """

        os.close(self.fileno())
        pcl_expect.Expectable.close(self)
        if hasattr(self, "stderr"):
            os.close(self.stderr.fileno())
            self.stderr.close()
        return os.waitpid(self.__child_pid, 0)

def spawn2(cmd, *args, **kwargs):
    """Spawn a process via a pty, but send stderr via a pipe.

       See Spawn.__init__ for a description of the arguments.

       This function returns a tuple of two instances:

        - a Spawn instance that can be used to send data to stdin of
          the process and to read the stdout from the process.

        - an Expectable instance that can be used to read the stderr
          from the process.

       To clean up once the process terminates, you should call the
       close() method of both objects.

       Please see the module doc string for more information including
       some caveats.

    """

    proc = Spawn(cmd, use_stderr_pipe=True, *args, **kwargs)
    return proc, proc.stderr
