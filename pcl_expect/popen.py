# pcl-expect: expect for Python.  Interface to os.popen.
# Copyright (C) 2003  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""Start a process using os.popen, and read input from it.
"""


import os

import pcl_expect

__all__ = [
    "Popen",
    ]

class Popen(pcl_expect.Expectable):
    def __init__(self, command):
        """Run command via os.popen, reading its output.
        """
        self.__cmd = os.popen(command, "r")
        pcl_expect.Expectable.__init__(self, self.__cmd.fileno())

    def close(self):
        """Close the pipe and wait for the child to die.

           Return the status of the child; see the documentation for
           os.popen for information about the return value.

           Note: this will block until the child process dies.
        """

        pcl_expect.Expectable.close(self)
        return self.__cmd.close()
