# pcl-expect: expect for Python.  Main module and base clases.
# Copyright (C) 2003  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""An extensible expect module with more expect-like feeling.

FIXME: more doc needed.
"""

import os
import re
import errno
import select
import sys

__all__ = [
    "timeout",
    "timeout_raises_exception",
    "RE",
    "EOF",
    "TIMEOUT",
    "BREAK",
    "Timeout",
    "BadArgs",
    "Expectable",
    "Controller",
    "expect_after",
    "expect_before",
    "expect",
    ]

# Default timeout, in seconds, as a floating point number.
# The user is supposed to change this as needed.
timeout = 10.0

# If a timeout occurs and no timeout handler is specified, should an
# exception be raised, or should the code just continue (TCL-style)?
# Default is to raise an exception.  The user is supposed to change
# this as needed.
timeout_raises_exception = True

# Constants that identify actions for expect_after and expect_before.
RE = 0
EOF = 1
TIMEOUT = 2

# Return value from callbacks.
BREAK = 3

class Timeout(Exception): pass
class BadArgs(Exception): pass

_expect_before = []
_expect_after = []

def debug(s):
    if 0:
        sys.stderr.write("pcl-expect: %s\n" % s)

class Expectable:
    """Base class for something you can expect input from.

       This class is usable as-is if you want to expect input from a
       file descriptor.  Derived classes provide more specialised
       access:

         popen.popen -- read from a command started with os.popen.
         spawn.spawn -- fork a command using a pty, the traditional
                        expect way.
         telnet.telnet -- use telnetlib.Telnet to connect to a remote
                        system.
         user.user   -- read input from stdin.

       This class expects input from a filedescriptor.  It uses
       os.read() to read input.  The file descriptor will not be
       closed by this class.

       Derived classes should define these methods:

        - __init__() -- extend it as appropriate
        - close()    -- extend it and release any resources
                        allocated in __init__().
        - _read()    -- override if os.read() is not appropriate
        - send()     -- define if it is possible to send data to the
                        object.

       The following methods are provided by the base class and
       derived classes should normally not have to alter them:

        - fileno()   -- return the file number used for reading.
        - fill_buffer() -- call _read and store the result in the buffer.
        - eof()      -- return True once eof() has been seen.
        - buffer()   -- return the current contents of the buffer.
        - remove_first() -- remove the first N bytes from the buffer.

       The following attributes are set when a match occurs:

        - match    -- A MatchObject from the re module.
        - consumed -- The string that was consumed by the re or
                      timeout condition.

    """

    def __init__(self, fileno):
        """Create an Expectable that reads from file descriptor fileno."""
        self.__fileno = fileno
        self.__buffer = ""
        self.__eof_seen = False

    def close(self):
        """Release all resources allocated by this Expectable."""
        self.__fileno = None
        self.__buffer = ""
        self.__eof_seen = True

    def fileno(self):
        """Return file descriptor number used by this Expectable."""
        return self.__fileno

    def _read(self):
        """Read some input.

           This method is called when more input is needed to match
           the expected target, and select indicates that there is
           something to read.  Derived classes may override this.

           This method should return a tuple of two items:
             - the read data as a string
             - an EOF flag

           Once this methods returns a true eof flag the method will
           not be called again.

           The default implementation uses os.read() to read a block
           of data from the file descriptor indicated by
           self.fileno().  EIO is handled as end-of-file.

        """

        try:
            s = os.read(self.fileno(), 8192)
        except OSError as e:
            if e.errno == errno.EIO:
                debug("got EIO from fd %d" % self.fileno())
                s = ""
            else:
                raise
        if s == "":
            debug("got eof from fd %d" % self.fileno())
            return "", True
        else:
            return s, False

    def fill_buffer(self):
        """Fill the input buffer.

           This method should only be called when select() indicates
           that there is pending input.  It will call _read() to do
           the actual reading.

           Once EOF has been seen, this method will do nothing and
           return True.  Otherwise, it will return False.
        """

        if not self.__eof_seen:
            s, eof = self._read()
            if eof:
                self.__eof_seen = True
            else:
                debug("got %d bytes from fd %d" % (len(s), self.fileno()))
                self.__buffer = self.__buffer + s
        else:
            debug("eof already seen on fd %d; not reading" % self.fileno())
        return self.__eof_seen

    def eof(self):
        """Return True if end-of-file has been seen.

           There might still be pending data in the buffer, though.
        """
        return self.__eof_seen

    def buffer(self):
        """Return the current contents of the buffer."""
        return self.__buffer

    def remove_first(self, n):
        """Remove the first n bytes from the buffer."""
        self.__buffer = self.__buffer[n:]


class Controller:
    """Loop controller object.

       This class is intended to be used to control a while loop that
       contains a single if...elif...elif statement, like this:

           x = pcl_expect.Controller()
           while x.loop():
               if x.<method>(...):
                   ...
               elif x.<method>(...):
                   ...

       <method> is one of the methods below.  In the following list,
       exp is an instance of Expectable or any of its derived
       classes.

        - timeout()  -- Returns true if a timeout occurs.

        - eof(exp)   -- Returns true if end-of-file has been reached.

        - re(exp, r) -- Returns true if the output from exp matches
                        the regular expression r.

       The loop will exit for the following reasons:

        - The user code uses break, return or an exception to break
          out of the loop.

        - An expect_before or expect_after pattern with no callback
          function matched.

        - An expect_before or expect_after pattern with a callback
          function matched, and the callback function returned BREAK.

        - A timeout occurs, and the timeout() method isn't called in
          any of the if...elif conditions, and the TIMEOUT pattern
          isn't present in any of the expect_before or expect_after
          patterns.  By default, this will raise a Timeout exception.
          It can also simply terminate the loop if the global variable
          timeout_raises_exception is set to False, or if the
          timeout_raises_exception argument of the constructor is set
          to False..

    """

    def __init__(self, timeout = None, timeout_raises_exception = None):
        """Create a loop-control object for expect.

           There are two optional arguments:

            - timeout -- A timeout value in seconds.  This overrides
                         the global timeout variable for this
                         particular loop.

            - timeout_raises_exception -- Should an unhandled timeout
                         raise an exception or simply terminate the
                         loop?  The default is to look at the global
                         variable timeout_raises_exception.

        """

        self.__first = True
        self.__inputs = set()
        self.__readable = set()
        self.__timeout = timeout
        self.__timeout_raises_exception = timeout_raises_exception
        self.__timeout_active = False
        self.__acted = False
        self.__all_inputs_seen = False
        debug("START")

    def loop(self):
        """Loop-controlling method.

           This method will handle expect_before and expect_after
           patterns.  It will block for new input using select when
           that is appropriate.
        """
        debug("at top of loop")

        if self.__first:
            self.__first = False
            debug("first time round")
            self.__acted = False
            if self.__expect_before():
                return False
            return True
        elif not self.__acted:
            debug("no action taken during previous loop")
            self.__all_inputs_seen = True
            if self.__expect_after():
                return False

        # Nothing handled the timeout event.
        if self.__timeout_active:
            debug("Unhandled timeout")

            if self.__timeout_raises_exception is not None:
                tre = self.__timeout_raises_exception
            else:
                tre = timeout_raises_exception

            if tre:
                raise Timeout()
            else:
                return False

        r = ", ".join([str(x.fileno()) for x in self.__inputs])
        timeout_possible = True
        if not self.__all_inputs_seen or self.__acted:
            timeout_possible = False
            t = 0
        elif self.__timeout == None:
            t = timeout
        else:
            t = self.__timeout
        if t > 0:
            debug("Waiting for input on %s" % r)
        else:
            debug("Polling for input on %s" % r)
        (r, w, e) = select.select(list(self.__inputs), [], [], t)

        self.__acted = False

        self.__readable = set(r)
        if len(self.__readable) == 0:
            if timeout_possible:
                debug("Processing timeout event")
                self.__timeout_active = True
        else:
            debug("Input available")
        if self.__expect_before():
            return False
        return True


    def __fill_buffer(self, exp):
        # Read data from exp if select indicates that there is data
        # available.
        if exp in self.__readable:
            debug("reading from fd %d" % exp.fileno())
            self.__readable.remove(exp)
            if exp.fill_buffer():
                debug("got eof on fd %d" % exp.fileno())
                self.__inputs.discard(exp)

    def re(self, exp, regexp):
        """Search for regexp in the output of exp.

           exp -- An instance of Expectable or any of its derived
                  classes.
           regexp -- A string, which will be compiled using
                  re.compile, or a precompiled re object.

           Return False if the regexp was not found.  Otherwise, the
           inital part of the buffer of exp up to and including the
           match for the regular expression is removed, the attributes
           below are set on the exp object, and True is returned:

            - match    -- The MatchObject.
            - consumed -- The string that was removed from the buffer
                          of exp.
        """

        if self.__acted:
            return False

        self.__fill_buffer(exp)

        debug("does %s match %s (fd %d)?" % (
            repr(exp.buffer()), repr(regexp), exp.fileno()))

        # Doing this compilation again and again could be a problem.
        # I rely on the cache in module re.  I hope it exists...
        if isinstance(regexp, str):
            regexp = re.compile(regexp)

        match = regexp.search(exp.buffer())
        if match != None:
            debug("yes")
            exp.match = match
            exp.consumed = exp.buffer()[:match.end()]
            exp.remove_first(match.end())
            self.__acted = True
            return True
        else:
            debug("no")
            if not exp.eof():
                debug("adding fd %d to rd-set" % exp.fileno())
                self.__inputs.add(exp)
            return False

    def eof(self, exp):
        """Check if end-of-file has been reached.

           exp -- An instance of Expectable or any of its derived
                  classes.

           Return False if end-of-file hasn't been reached yet.
           Otherwise, the buffer of exp is emptied, the following
           attributes of the exp object are set, and True is returned:

            - match    -- Set to None.
            - consumed -- Set to the string that was removed from the
                          buffer of exp.
        """
        if self.__acted:
            return False
        
        self.__fill_buffer(exp)

        if exp.eof():
            debug("eof seen on fd %d" % exp.fileno())
            exp.match = None
            exp.consumed = exp.buffer()
            exp.remove_first(len(exp.consumed))
            self.__acted = True
            return True
        else:
            debug("no eof on fd %d" % exp.fileno())
            debug("adding fd %d to rd-set" % exp.fileno())
            self.__inputs.add(exp)
            return False

    def timeout(self):
        """Return true if a timeout has occured."""

        if self.__acted:
            return False
        
        if self.__timeout_active:
            debug("eof pending")
            self.__timeout_active = False
            self.__acted = True
            return True
        else:
            debug("no eof pending")
            return False

    def __expect_before(self):
        # Run expect_before patterns.  Return True if a pattern
        # matched and returned BREAK.
        debug("running expect_before patterns...")
        ret = self.__run_expectations(_expect_before)
        debug("done running expect_before patterns: %s." % ret)
        return ret

    def __expect_after(self):
        # Run expect_after patterns.  Return True if a pattern matched
        # and returned BREAK.
        debug("running expect_after patterns...")
        ret = self.__run_expectations(_expect_after)
        debug("done running expect_after patterns: %s." % ret)
        return ret

    def __run_expectations(self, expectations):
        # Run the expect_after or expect_before patterns in
        # expectations.  Return True if a pattern matched and returned
        # BREAK.
        for pattern in expectations:
            debug("running pattern %s" % (pattern,))
            if pattern[0] == RE:
                (cmd, exp, regexp, callback) = pattern
                if self.re(exp, regexp):
                    return callback(exp) == BREAK
            elif pattern[0] == EOF:
                (cmd, exp, callback) = pattern
                if self.eof(exp):
                    return callback(exp) == BREAK
            elif pattern[0] == TIMEOUT:
                (cmd, callback) = pattern
                if self.timeout():
                    return callback() == BREAK
            else:
                raise BadArgs
            debug("no match")
        return False


def expect_after(expectations):
    """Set a list of expect patterns to run if no other matches are found.

       The argument is a list of tuples.  These tuples are understood:
       
           (pcl_expect.RE, exp, regexp, callback)

               If output from exp matches regexp, call callback.  The
               regexp can be either a string (which will be compiled
               using re.compile()) or a precompiled regexp.  See
               Controller.re() for info about attributes that are set
               on exp.

           (pcl_expect.EOF, exp, callback)

               If end-of-file is reached on exp, call callback.  See
               Controller.timeout() for info about attributes that are
               set on exp.

           (pcl_expect.TIMEOUT, callback)

               If a timeout occurs, call callback.

       The callback function is called with exp as argument (or, for
       timeout, with no argument).  It should return None or the
       constant pcl_expect.BREAK.  Returning BREAK from a callback
       means that the expect statement should not start again.

       Sample use:

           def timeout_cb():
               sys.stderr.write("timeout occured\n")
               sys.exit(1)

           pcl_expect.expect_after([(pcl_expect.TIMEOUT, timeout_cb)])

       The above example will cause timeouts to exit the process after
       printing a message.  Both timeouts during a Controller-
       controlled loop and during a call to pcl_expect.expect will be
       affected.

    """

    global _expect_after

    __validate_expectations(expectations)
    _expect_after = expectations
    debug("Added expect_after patterns")

def expect_before(expectations):
    """Set a list of expect patterns to run before looking at other matches.

       See expect_after for more info.  The patterns supplied via this
       function will be tried before the patterns specified in a
       specific call to pcl_expect.expect or in a Controller-
       controlled loop.

    """
    global _expect_before

    __validate_expectations(expectations)
    _expect_before = expectations
    debug("Added expect_before patterns")


def __validate_expectations(expectations):
    # Check that the expectations follow the rules documented in
    # expect_after, so that an error can be given early on.
    for pattern in expectations:
        if pattern[0] == RE:
            (cmd, exp, regexp, callback) = pattern
        elif pattern[0] == EOF:
            (cmd, exp, callback) = pattern
        elif pattern[0] == TIMEOUT:
            (cmd, callback) = pattern
        else:
            raise BadArgs

def expect(expectations):
    """Wait for input.

       The Controller class provides an alternative API, which the
       author of pcl_expect believes produces more readable code.
       This function provides an API which is closer to the previous
       expect-like Python modules.

       The argument is a list of tuples.  These tuples are understood:
       
           (pcl_expect.RE, exp, regexp[, callback])

               If output from exp matches regexp, call callback.  The
               regexp can be either a string (which will be compiled
               using re.compile()) or a precompiled regexp.  See
               Controller.re() for info about attributes that are set
               on exp.

           (pcl_expect.EOF, exp[, callback])

               If end-of-file is reached on exp, call callback.  See
               Controller.timeout() for info about attributes that are
               set on exp.

           (pcl_expect.TIMEOUT, [callback])

               If a timeout occurs, call callback.

       The callback function is called with exp as argument (or, for
       timeout, with no argument).  It should return None or the
       constant pcl_expect.BREAK.  The function will return once a
       callback returns pcl_expect.BREAK, or when a callback supplied
       to expect_before() or expect_after() returns pcl_expect.BREAK.

       The callback can be omitted.  It defaults to a function that
       returns pcl_expect.BREAK.

       Sample use:

           def timeout_cb():
               sys.stderr.write("timeout occured\n")
               sys.exit(1)

           pcl_expect.expect([(pcl_expect.RE, exp, "foo"),
                              (pcl_expect.TIMEOUT, timeout_cb)])

       The above example will return as soon as the output from exp
       matches "foo".  If a timeout occurs, the process will exit
       after printing a message.

       This will return a tuple of two values:

        - The index in the argument list that causes expect() to
          return, or None if it returns due to a expect_before or
          expect_after callback that returns pcl_expect.BREAK.  None
          will also be returned if an unhandled timeout occurs and
          timeout_raises_exception is set to False.

        - The Expectable object that caused expect() to return, or
          None if it returned because of a timeout.  If the first
          value is None, the second value will also be None.

    """
    x = Controller()
    while x.loop():
        for ix in range(len(expectations)):
            pattern = expectations[ix]
            if pattern[0] == RE:
                if len(pattern) == 3:
                    (cmd, exp, regexp) = pattern
                    callback = None
                elif len(pattern) == 4:
                    (cmd, exp, regexp, callback) = pattern
                else:
                    raise BadArgs
                if x.re(exp, regexp):
                    if callback is None or callback(exp) == BREAK:
                        return ix, exp
                    else:
                        break
            elif pattern[0] == EOF:
                if len(pattern) == 2:
                    (cmd, exp) = pattern
                    callback = None
                elif len(pattern) == 3:
                    (cmd, exp, callback) = pattern
                else:
                    raise BadArgs
                if x.eof(exp):
                    if callback is None or callback(exp) == BREAK:
                        return ix, exp
                    else:
                        break
            elif pattern[0] == TIMEOUT:
                if len(pattern) == 1:
                    cmd = pattern[0]
                elif len(pattern) == 2:
                    (cmd, callback) = pattern
                else:
                    raise BadArgs
                if x.timeout():
                    if callback is None or callback() == BREAK:
                        return ix, None
                    else:
                        break
    return None, None
