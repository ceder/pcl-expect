# pcl-expect: expect for Python.  Telnet interface.
# Copyright (C) 2003  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""Connect to a telnet server via the telnetlib.Telnet class.
"""

import telnetlib

import pcl_expect

__all__ = [
    "Telnet",
    ]

class Telnet(pcl_expect.Expectable):
    def __init__(self, host, port):
        """Establish a telnet session.

           The host and port argument are passed to telnetlib.Telnet.

        """

        self.telnet = telnetlib.Telnet(host, port)
        pcl_expect.Expectable.__init__(self, self.telnet.fileno())

    def _read(self):
        try:
            s = self.telnet.read_eager()
        except EOFError:
            return "", True
        return s, False

    def send(self, s):
        """Send a string to the remote telnet server."""
        self.telnet.write(s)

    def close(self):
        """Close the telnet session."""
        pcl_expect.Expectable.close(self)
        self.telnet.close()

