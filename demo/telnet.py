# pcl-expect: expect for Python.  Simple telnet demo.
# Copyright (C) 2003  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# Expects a host and user name on the command line, reads a password
# from stdin, logs in on that host, runs "uname -a", and prints the
# result.

import os
import sys

import pcl_expect
import pcl_expect.telnet

host = sys.argv[1]
user = sys.argv[2]

sys.stdout.write("Password for %s at %s? " % (user, host))
sys.stdout.flush()

try:
    os.system("stty -echo")
    password = sys.stdin.readline()
finally:
    os.system("stty echo")
print()

if len(password) > 0 and password[-1] == "\n":
    password = password[:-1]

t = pcl_expect.telnet.Telnet(host, "23")

x = pcl_expect.Controller()
while x.loop():
    if x.re(t, "ogin:"):
        t.send(user + "\n")
        break

x = pcl_expect.Controller()
while x.loop():
    if x.re(t, "assword:"):
        t.send(password + "\n")
        break

x = pcl_expect.Controller()
while x.loop():
    if x.re(t, ">|\\$|#"):
        t.send("uname -a\n")
        break
    elif x.re(t, "incorrect"):
        print("Wrong password")
        t.close()
        sys.exit(1)
        
x = pcl_expect.Controller()
while x.loop():
    if x.re(t, "(.*)\r?\n.*(>|\\$|#)"):
        print("SYSTEM ID:", t.match.group(1))
        t.send("exit\n")
        break
        
x = pcl_expect.Controller()
while x.loop():
    if x.eof(t):
        t.close()
        break
