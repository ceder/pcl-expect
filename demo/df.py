# pcl-expect: expect for Python.  Simple df parsing demo.
# Copyright (C) 2003  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from pcl_expect import Controller
from pcl_expect.popen import Popen

df = Popen("df -k")

x = Controller()
while x.loop():
    if x.re(df, "Filesystem.*\n"):
        print("Header:", df.match.group())
        break

x = Controller()
while x.loop():
    if x.re(df, "^/.* .*(?=\n)"):
        print("Normal line:", df.match.group())
    elif x.re(df, "^/.*(?=\n)"):
        print("Mount point only:", df.match.group())
    elif x.re(df, "^ .*(?=\n)"):
        print("Info only:", df.match.group())
    elif x.re(df, "^\n"):
        pass
    elif x.re(df, "^.*(?=\n)"):
        print("Unexpected line", df.match.group())
    elif x.eof(df):
        print("And that's all, folks!")
        break
