# pcl-expect: expect for Python.  The mandatory ftp example...
# Copyright (C) 2003  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import pcl_expect.spawn
import pcl_expect

pcl_expect.spawn.stty_init = "-onlcr -ocrnl"

ftp = pcl_expect.spawn.Spawn("ftp ftp.funet.fi")

x = pcl_expect.Controller()
while x.loop():
    if x.re(ftp, "Name.*:"):
        ftp.send("anonymous\n")
    elif x.re(ftp, "Password:"):
        ftp.send("ceder@lysator.liu.se\n")
    elif x.re(ftp, "ftp> "):
        break

ftp.send("cd pub\n")

x = pcl_expect.Controller()
while x.loop():
    if x.re(ftp, "(?s).*ftp> "):
        break

ftp.send("passive\n")

x = pcl_expect.Controller()
while x.loop():
    if x.re(ftp, "(?s).*ftp> "):
        break

ftp.send("dir\n")

x = pcl_expect.Controller()
while x.loop():
    if x.re(ftp, "(?s).*ftp> "):
        print(ftp.match.group())
        break

ftp.close()
