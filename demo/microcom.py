# pcl-expect: expect for Python.  Minimal terminal emulator.
# Copyright (C) 2003  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# This example opens the first serial port in 9600 bits per second and
# connects the keyboard to it.  It terminates when the user presses Ctrl-].

import pcl_expect.pyserial
import pcl_expect.user
import pcl_expect

# Open the serial port.  You could specify parity, number of bits, and
# a lot of other things here as well.  See the pySerial
# documentation.

port = pcl_expect.pyserial.Serial(0, baudrate=9600)

# We need to listen for input from the user as well.  Set the tty in
# RAW mode so that the user can enter Ctrl-C, DEL, Backspace and other
# special characters.

user = pcl_expect.user.User(pcl_expect.user.RAW)

x = pcl_expect.Controller()
while x.loop():
    if x.re(user, "^\035"):
        # Exit once the user hits Ctrl-].
        break
    elif x.re(user, "(?s)."):
        # Anything else is sent to the serial port.
        port.send(user.consumed)
    elif x.re(port, "(?s)..*"):
        # Anything that arrives from the serial port is sent to the
        # user.
        user.send(port.consumed)
    elif x.timeout():
        # Ignore timeouts.
        pass

port.close()
