# pcl-expect: expect for Python.  Demo that benchmarks two ftp servers.
# Copyright (C) 2003  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# A more complex ftp example.  This one connects to two ftp servers,
# does a "cd pub" on both, and prints a directory listing.  This is
# done in parallell, so the output from the fastest server will be
# printed first.  You could use this program as a kind of poor mans
# ftp server benchmark.
#
# This also tests the expect_after functionality.

import sys
import time

import pcl_expect
import pcl_expect.spawn

pcl_expect.spawn.stty_init = "-onlcr -ocrnl -echo"

time0 = time.time()
funet = pcl_expect.spawn.Spawn("ftp ftp.funet.fi")
sunet = pcl_expect.spawn.Spawn("ftp ftp.sunet.se")

def name_cb(ftp):
    ftp.send("anonymous\n")

def password_cb(ftp):
    ftp.send("ceder@lysator.liu.se\n")

cmds = {}
cmds[funet] = ["cd pub\n", "passive\n", "dir\n"]
cmds[sunet] = ["cd pub\n", "passive\n", "dir\n"]

speed = {}

printed = 0
def prompt_cb(ftp):
    global printed

    if len(cmds[ftp]) > 0:
        ftp.send(cmds[ftp][0])
        cmds[ftp] = cmds[ftp][1:]
    else:
        print(ftp.match.group())
        printed += 1
        speed[ftp] = time.time() - time0
            
    pcl_expect.debug("STATE: cmds[sunet] = %s\n" % cmds[sunet])
    pcl_expect.debug("STATE: cmds[funet] = %s\n" % cmds[funet])

    if printed == 2:
        return pcl_expect.BREAK

pcl_expect.expect_after([(pcl_expect.RE, funet, "Name.*:", name_cb),
                         (pcl_expect.RE, sunet, "Name.*:", name_cb),
                         (pcl_expect.RE, funet, "Password:", password_cb),
                         (pcl_expect.RE, sunet, "Password:", password_cb),
                         (pcl_expect.RE, funet, "(?s).*ftp> ", prompt_cb),
                         (pcl_expect.RE, sunet, "(?s).*ftp> ", prompt_cb)])
              
x = pcl_expect.Controller()
while x.loop():
    if x.timeout():
        sys.stdout.write(".")
        sys.stdout.flush()

sunet.send("bye\n")
funet.send("bye\n")

pcl_expect.expect_after([])

x = pcl_expect.Controller()
while x.loop():
    if x.eof(sunet):
        print("SUNET final output:", sunet.consumed)
        sunet.close()
        break
    elif x.timeout():
        sys.stdout.write(".")
        sys.stdout.flush()

x = pcl_expect.Controller()
while x.loop():
    if x.eof(funet):
        print("FUNET final output:", funet.consumed)
        funet.close()
        break
    elif x.timeout():
        sys.stdout.write(".")
        sys.stdout.flush()

print("SUNET time:", speed[sunet], "seconds")
print("FUNET time:", speed[funet], "seconds")
