# pcl-expect: expect for Python.  Demo serial code.
# Copyright (C) 2003  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# This example opens the first serial port in 4800 bits per second and
# reads input from it.  It terminates after 10 consecutive seconds
# with no input.

import pcl_expect.pyserial
import pcl_expect

port = pcl_expect.pyserial.Serial(0, baudrate=4800)

x = pcl_expect.Controller()
while x.loop():
    if x.re(port, "..*"):
        print("Got", repr(port.consumed))
    elif x.timeout():
        print("Timeout")
        break

port.close()
