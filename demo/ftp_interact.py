# pcl-expect: expect for Python.  Demo interactive ftp program.
# Copyright (C) 2003  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# An interactive ftp example.
# This will connect to ftp.funet.fi.  The command "home" will cd
# to one of the most useful areas: /pub/gnu/ftp.gnu.org/pub/gnu

import pcl_expect.spawn
import pcl_expect.user
import pcl_expect

def prompt():
    x = pcl_expect.Controller()
    while x.loop():
        if x.re(ftp, "(?s).*ftp> "):
            break

def cmd(s):
    ftp.send(s + "\n")
    prompt()

pcl_expect.spawn.stty_init = "-onlcr -ocrnl -echo"

ftp = pcl_expect.spawn.Spawn("ftp ftp.funet.fi")

x = pcl_expect.Controller()
while x.loop():
    if x.re(ftp, "Name.*:"):
        ftp.send("anonymous\n")
    elif x.re(ftp, "Password:"):
        ftp.send("ceder@lysator.liu.se\n")
    elif x.re(ftp, "ftp> "):
        break

cmd("cd pub")
cmd("cd gnu")
cmd("cd ftp.gnu.org")
cmd("cd pub")
cmd("cd gnu")
cmd("passive")

user_spawn_id = pcl_expect.user.User()
user_spawn_id.send("ftp> ")
buf = ""

x = pcl_expect.Controller()
while x.loop():
    if x.re(user_spawn_id, "..*"):
        s = user_spawn_id.match.group()
        buf += s
    elif x.re(user_spawn_id, "\n"):
        if buf == "home":
            cmd("cd /pub/gnu/ftp.gnu.org/pub/gnu")
            user_spawn_id.send("ftp> ")
        else:
            ftp.send(buf + "\n")
        buf = ""
    elif x.eof(user_spawn_id):
        user_spawn_id.send("\nGot eof from stdin\n")
        break
    elif x.re(ftp, "(?s)..*"):
        user_spawn_id.send(ftp.match.group())
    elif x.eof(ftp):
        user_spawn_id.send("\nGot eof from ftp process\n")
        break
    elif x.timeout():
        pass
