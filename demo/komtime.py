# pcl-expect: expect for Python.  LysKOM Protocol A get-time demo.
# Copyright (C) 2003  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# Connect to the LysKOM server at kom.lysator.liu.se, query it for the
# current time, and print the result in ISO 8601 format.

import pcl_expect.tcp
import pcl_expect

kom = pcl_expect.tcp.TcpClient(("kom.lysator.liu.se", 4894))

kom.send("A20Hpcl-expect time demo\n")

x = pcl_expect.Controller()
while x.loop():
    if x.re(kom, "LysKOM\n"):
        break

kom.send("0 35\n")
x = pcl_expect.Controller()
while x.loop():
    if x.re(kom, ("=0 "
                  "(?P<sec>[0-9]*) "
                  "(?P<min>[0-9]*) "
                  "(?P<hour>[0-9]*) "
                  "(?P<day>[0-9]*) "
                  "(?P<month>[0-9]*) "
                  "(?P<year>[0-9]*) "
                  "(?P<wday>[0-9]*) "
                  "(?P<yday>[0-9]*) "
                  "(?P<isdst>[0-9]*)"
                  "\n")):
        year, month, day, hour, min, sec = [int(x) for x in kom.match.group(
            "year", "month", "day", "hour", "min", "sec")]
        print("%4d-%02d-%02d %02d:%02d:%02d (according to the server)" % (
            1900+year, 1+month, day, hour, min, sec))
        break

kom.send("1 55 0\n")
x = pcl_expect.Controller()
while x.loop():
    if x.re(kom, "=1\n"):
        pass
    elif x.re(kom, ":2 .*\n"):
        pass
    elif x.eof(kom):
        if kom.consumed != "":
            print("Unexpected output:", kom.consumed)
        break

kom.close()
