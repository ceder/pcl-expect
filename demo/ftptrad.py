# pcl-expect: expect for Python.  Demo ftp program of the expect function.
# Copyright (C) 2003  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# The mandatory ftp example, in traditional style.

import pcl_expect
import pcl_expect.spawn

pcl_expect.spawn.stty_init = "-onlcr -ocrnl"

ftp = pcl_expect.spawn.Spawn("ftp ftp.funet.fi")

def send_anon(exp):
    exp.send("anonymous\n")

def send_user(exp):
    exp.send("ceder@lysator.liu.se\n")

ret, exp = pcl_expect.expect([
    (pcl_expect.RE, ftp, "Name.*:", send_anon),
    (pcl_expect.RE, ftp, "Password.*:", send_user),
    (pcl_expect.RE, ftp, "ftp> "),
    ])

assert ret == 2
assert exp == ftp

ftp.send("cd pub\n")

pcl_expect.expect([(pcl_expect.RE, ftp, "(?s).*ftp> ")])

ftp.send("passive\n")

pcl_expect.expect([(pcl_expect.RE, ftp, "(?s).*ftp> ")])

ftp.send("dir\n")

ret, exp = pcl_expect.expect([(pcl_expect.RE, ftp, "(?s).*ftp> ")])
if ret == 0:
    print(ftp.match.group())

ftp.close()
