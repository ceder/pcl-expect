# pcl-expect: expect for Python.  Spawn demo program.
# Copyright (C) 2003  Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os

import pcl_expect.spawn
import pcl_expect.user
import pcl_expect

pcl_expect.spawn.stty_init = "-onlcr -ocrnl"

sh = pcl_expect.spawn.Spawn(os.getenv("SHELL"))
user = pcl_expect.user.User(pcl_expect.user.CBREAK)

x = pcl_expect.Controller()
while x.loop():
    if x.re(user, "(?s)..*"):
        sh.send(user.consumed)
    elif x.re(sh, "(?s)..*"):
        # user.send("STDOUT: ``%s''.\n" % repr(sh.consumed))
        user.send(sh.consumed)
    elif x.timeout():
        pass
    elif x.eof(user):
        break
    elif x.eof(sh):
        break

sh.close()
