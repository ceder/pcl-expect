#! /usr/bin/env python

import os
import sys

import unittest

def findsuites():
    suites = []
    for fn in os.listdir('test'):
        if fn.startswith('test_') and fn.endswith('.py'):
            suites.append('test.' + fn[:-3])
    suites.sort()
    return suites

def my_import(name):
    mod = __import__(name)
    components = name.split('.')
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod

def main():
    suites = findsuites()
    all = unittest.TestSuite()
    load = unittest.defaultTestLoader.loadTestsFromModule
    for suite in suites:
        all.addTest(load(my_import(suite)))
    res = unittest.TextTestRunner().run(all)
    res.stream = unittest._WritelnDecorator(open("test.result", "w"))
    res.printErrors()
    return not res.wasSuccessful()

if __name__ == '__main__':
    main()
