#!/usr/bin/env python3

from distutils.core import setup

classifiers="""\
Development Status :: 3 - Alpha
Environment :: Console
Environment :: No Input/Output (Daemon)
Environment :: Other Environment
Intended Audience :: Developers
Intended Audience :: System Administrators
License :: OSI Approved :: GNU General Public License (GPL)
Natural Language :: English
Operating System :: POSIX
Programming Language :: Python
Topic :: Communications
Topic :: Internet
Topic :: Software Development
Topic :: Software Development :: Libraries :: Python Modules
Topic :: Software Development :: Testing
Topic :: Terminals :: Serial
Topic :: Terminals :: Telnet
"""

setup(name="pcl-expect",
      version="0.1.0",
      description="Pure Python expect module",
      author="Per Cederqvist",
      author_email="ceder@lysator.liu.se",
      url="http://www.lysator.liu.se/~ceder/pcl-expect/",
      packages=['pcl_expect'],
      license="GNU GPL",
      long_description="""
      Pcl-expect is a pure Python expect-like module, with a few twists.
      For example, not only can you talk to processes via a pty, but you can
      also talk to a Telnet server, any TCP server, a serial port, et c.
      """,
      classifiers = [_f for _f in classifiers.split("\n") if _f],
      platforms = ["POSIX"],
      keywords = "expect pcl-expect pty telnet serial pipe tcp popen"
     )
