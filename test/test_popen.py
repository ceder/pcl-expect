import time

import pcl_expect
from pcl_expect import Controller
from pcl_expect.popen import Popen

import test.base

class TestPopen(test.base.TestCase):
    def test_popen_sleep_1(self):
        t0 = time.time()
        sleeper = Popen('sleep 1')
        x = Controller()
        while x.loop():
            if sleeper.re(x, '.'):
                self.fail('got output from sleep')
            elif x.timeout():
                self.fail('timeout waiting for sleep to terminate')
            elif sleeper.eof(x):
                break
        status = sleeper.close()
        self.assertEqual(status, None)
        t1 = time.time()
        self.assertTimeDiff(t0, t1, 1.0)

    def test_popen_sleep_1_compat(self):
        t0 = time.time()
        sleeper = Popen('sleep 1')
        x = Controller()
        while x.loop():
            if x.re(sleeper, '.'):
                self.fail('got output from sleep')
            elif x.timeout():
                self.fail('timeout waiting for sleep to terminate')
            elif x.eof(sleeper):
                break
        status = sleeper.close()
        self.assertEqual(status, None)
        t1 = time.time()
        self.assertTimeDiff(t0, t1, 1.0)

    def test_read_hello(self):
        hello = Popen('echo hello, world')
        x = Controller()
        while x.loop():
            if hello.re(x, 'hello, world\n'):
                break
        x = Controller()
        while x.loop():
            if hello.eof(x):
                break
        status = hello.close()
        self.assertEqual(status, None)

    def test_read_hello_partial(self):
        hello = Popen('echo hello, world')
        x = Controller()
        while x.loop():
            if hello.re(x, 'hello'):
                break
        x = Controller()
        while x.loop():
            if hello.re(x, ', world'):
                break
        x = Controller()
        while x.loop():
            if hello.re(x, '\n'):
                break
        x = Controller()
        while x.loop():
            if hello.eof(x):
                break
        status = hello.close()
        self.assertEqual(status, None)

    def test_read_hello_compat(self):
        hello = Popen('echo hello, world')
        x = Controller()
        while x.loop():
            if x.re(hello, 'hello, world\n'):
                break
        x = Controller()
        while x.loop():
            if x.eof(hello):
                break
        status = hello.close()
        self.assertEqual(status, None)

    def test_read_hello_no_eof_handling(self):
        hello = Popen('echo hello, world')
        seen = False
        x = Controller()
        try:
            while x.loop():
                if hello.re(x, 'hello, world\n'):
                    if seen:
                        self.fail("got hello twice")
                    seen = 1
            self.fail('no exception raised')
        except pcl_expect.UnhandledEof:
            pass
        status = hello.close()
        self.assertEqual(status, None)
        self.assertEqual(seen, 1)

    def test_read_hello_no_eof_handling_no_exception(self):
        hello = Popen('echo hello, world')
        seen = False
        env = pcl_expect.Environment()
        env.set_eof_raises_exception(False)
        x = env.controller()
        while x.loop():
            if hello.re(x, 'hello, world\n'):
                if seen:
                    self.fail("got hello twice")
                seen = 1
        status = hello.close()
        self.assertEqual(status, None)
        self.assertEqual(seen, 1)

