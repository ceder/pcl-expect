import time

import pcl_expect
import pcl_expect.spawn

import test.base

class TestSpawn(test.base.TestCase):
    def test_spawn_sleep_1(self):
        t0 = time.time()
        sleeper = pcl_expect.spawn.Spawn(['sleep', '1'])
        x = pcl_expect.Controller()
        while x.loop():
            if sleeper.re(x, '.'):
                self.fail('got output from sleep')
            elif x.timeout():
                self.fail('timeout waiting for sleep to terminate')
            elif sleeper.eof(x):
                break
        pid, status = sleeper.close()
        self.assertEqual(status, 0)
        t1 = time.time()
        self.assertTimeDiff(t0, t1, 1.0)

    def test_spawn_sleep_1_compat(self):
        t0 = time.time()
        sleeper = pcl_expect.spawn.Spawn(['sleep', '1'])
        x = pcl_expect.Controller()
        while x.loop():
            if x.re(sleeper, '.'):
                self.fail('got output from sleep')
            elif x.timeout():
                self.fail('timeout waiting for sleep to terminate')
            elif x.eof(sleeper):
                break
        pid, status = sleeper.close()
        self.assertEqual(status, 0)
        t1 = time.time()
        self.assertTimeDiff(t0, t1, 1.0)


    def test_hello_world(self):
        hw = pcl_expect.spawn.Spawn(['test/hello.sh'])
        x = pcl_expect.Controller()
        while x.loop():
            if hw.re(x, "hello, world"):
                break
        x = pcl_expect.Controller()
        while hw.eof(x):
            break
        pid, status = hw.close()
        self.assertEqual(status, 0)

    def test_stderr(self):
        tool = pcl_expect.spawn.Spawn("test/tool.sh")
        x = pcl_expect.Controller()
        while x.loop():
            if tool.re(x, "prompt: "):
                break
        tool.send("echo hello, world\n")
        x = pcl_expect.Controller()
        while x.loop():
            if tool.re(x, "hello, world\r?\n"):
                break
        tool.send("stderr hello, world\n")
        x = pcl_expect.Controller()
        while x.loop():
            if tool.re(x, "hello, world\r?\n"):
                break
        tool.send("bye\n")
        found = 0
        x = pcl_expect.Controller()
        while x.loop():
            if tool.eof(x):
                break
            elif tool.re(x, "Exiting\r?\n"):
                found += 1
        self.assertEqual(found, 1)
        pid, status = tool.close()
        self.assertEqual(status, 0)

    def test_stderr_2(self):
        (tool, stderr) = pcl_expect.spawn.spawn2("test/tool.sh")
        x = pcl_expect.Controller()
        while x.loop():
            if tool.re(x, "prompt: "):
                break
        tool.send("echo hello, world\n")
        x = pcl_expect.Controller()
        while x.loop():
            if tool.re(x, "hello, world\r?\n"):
                break
        tool.send("stderr hello, world\n")
        x = pcl_expect.Controller()
        while x.loop():
            if stderr.re(x, "hello, world\r?\n"):
                break
        tool.send("bye\n")
        found = 0
        x = pcl_expect.Controller()
        while x.loop():
            if tool.eof(x):
                break
            elif tool.re(x, "Exiting\r?\n"):
                found += 1
        self.assertEqual(found, 1)
        x = pcl_expect.Controller()
        while x.loop():
            if stderr.eof(x):
                break
        pid, status = tool.close()
        self.assertEqual(status, 0)
        stderr.close()
