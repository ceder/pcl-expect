#!/bin/sh
n=-n
e=
while :
do
  echo $n "prompt: $e"
  read cmd args
  if test "$cmd" = echo
  then
      echo You wrote: $args
  elif test "$cmd" = stderr
  then
      echo You wrote: $args >&2
  elif test "$cmd" = bye
  then
      echo Exiting
      exit 0
  else
      echo unknown command: $cmd
      exit 1
  fi
done
