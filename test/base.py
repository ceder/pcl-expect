import signal
import unittest

import pcl_expect

class Timeout(Exception): pass

def handler(signo, stk):
    raise Timeout()

class TestCase(unittest.TestCase):
    """Base class for most pcl-expect test suites.
    """
    def setUp(self):
        """Reset state in pcl_expect, and arm the timeout."""
        pcl_expect.reset_state()
        signal.signal(signal.SIGALRM, handler)
        self.progress()

    def tearDown(self):
        """Cancel the timeout."""
        signal.alarm(0)

    def progress(self):
        """Re-arm the timeout.

        Test cases that are expected to take a long time to run may
        use this method -- but only if they are absolutely certain
        that they cannot be running in an endless loop.
        """

        signal.alarm(20)

    def assertTimeDiff(self, t0, t1, expected):
        """Report an error unless two times differ the expected amount.

        Compute t1-t0, and compare it to expected.  If they the
        expected differance is more than 0.2 seconds off, raise an
        error.
        """

        diff = t1-t0
        err = abs(diff-expected)
        if err > 0.2:
            self.fail('wrong time difference %g, expected %g' % (
                diff, expected))
